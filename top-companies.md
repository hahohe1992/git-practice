| Rank | Name | Industry | Revenue(USD millions) | Profit(USD millions) | Employees | Country | Ref |
| ---- | ---- | -------- | --------------------- | -------------------- | --------- | ------- | --- |
| 1 | Walmart | Retail | $514,405 | Increase $6,670 | 2,200,000 | United States | United States	[5] |
| 2 | Sinopec | Oil and gas | $414,649 | Increase $5,845 | 619,151 | China | China [6] |
| 3 | Royal Dutch Shell | Oil and gas | $396,556 | Increase $23,352 | 81,000 | Netherlands | Netherlands United Kingdom United Kingdom	[7] |
| 4 | China National Petroleum | Oil and gas | $392,976 | Increase $2,270 | 1,382,401 | China | China [8] |
| 5 | State Grid | Electricity | $387,056 | Increase $8,174 | 917,717 | China | China [9] |
| 6 | Saudi Aramco | Oil and gas | $355,905 | Increase $110,974 | 76,418 | Saudi Arabia | Saudi Arabia [10] |
| 7 | BP | Oil and gas | $303,738 | Increase $9,383 | 73,000 | United Kingdom | United Kingdom [11] |
| 8 | ExxonMobil | Oil and gas | $290,212 | Increase $20,840 | 71,000 | United States | United States [12] |
| 9	| Volkswagen | Automotive | $278,341 | Increase $14,332 | 664,496 | Germany | Germany | [13] |
| 10 | Toyota | Automotive | $272,612 | Increase $16,982 | 370,870 | Japan | Taiwan | [14] |
11	Apple	Electronics	$265,595	Increase $59,531	132,000	United States United States	[15]
12	Berkshire Hathaway	Conglomerate	$247,837	Increase $4,021	389,000	United States United States	[16]
13	Amazon	Retail	$232,887	Increase $10,073	647,000	United States United States	[17]
14	UnitedHealth	Healthcare	$226,247	Increase $11,986	300,000	United States United States	[18]
15	Samsung Electronics	Electronics	$221,579	Increase $39,895	221,579	South Korea South Korea	[19]
16	Glencore	Commodities	$219,754	Increase $3,408	85,504	Switzerland Switzerland	[20]
17	McKesson	Healthcare	$214,319	Increase $34	70,000	United States United States	[21]
18	Daimler	Automotive	$197,515	Increase $8,555	298,683	Germany Germany	[22]
19	CVS Health	Healthcare	$194,579	Decrease -$594	295,000	United States United States	[23]
20	Total	Oil and gas	$184,106	Increase $11,446	104,460	France France	[24]
21	China State Construction	Construction	$181,524	Increase $3,159	302,827	China China	[25]
22	Trafigura	Commodities	$180,744	Increase $849	4,316	Singapore Singapore	[26]
23	Foxconn	Electronics	$175,617	Increase $4,281	667,680	Taiwan Taiwan	[27]
24	Exor	Financials	$175,009	Increase $1,589	314,790	Netherlands Netherlands	[28]
25	AT&T	Telecommunications	$170,756	Increase $19,370	254,000	United States United States	[29]
26	ICBC	Financials	$168,979	Increase $45,002	449,296	China China	[30]
27	AmerisourceBergen	Pharmaceuticals	$167,939	Increase $1,658	20,500	United States United States	[31]
28	Chevron	Oil and gas	$166,339	Increase $14,824	48,600	United States United States	[32]
29	Ping An Insurance	Financials	$163,597	Increase $16,237	342,550	China China	[33]
30	Ford	Automotive	$160,338	Increase $3,677	199,000	United States United States	[34]
31	China Construction Bank	Financials	$151,110	Increase $38,498	366,996	China China	[35]
32	General Motors	Automotive	$147,049	Increase $8,014	173,000	United States United States	[36]
33	Mitsubishi	Conglomerate	$145,243	Increase $5,328	79,994	Japan Japan	[37]
34	Honda	Automotive	$143,302	Increase $5,504	219,772	Japan Japan	[38]
35	Costco	Retail	$141,576	Increase $3,134	194,000	United States United States	[39]
36	Agricultural Bank of China	Financials	$139,523	Increase $30,656	477,526	China China	[40]
37	Alphabet	Internet	$136,819	Increase $30,736	98,771	United States United States	[41]
38	Cardinal Health	Pharmaceuticals	$136,809	Increase $256	50,200	United States United States	[42]
39	SAIC Motor	Automotive	$136,392	Increase $5,443	147,738	China China	[43]
40	Walgreens Boots Alliance	Retail	$131,537	Increase $5,024	299,000	United States United States	[44]
41	JPMorgan Chase	Financials	$131,412	Increase $32,474	256,105	United States United States	[45]
42	Gazprom	Oil and gas	$131,302	Increase $23,199	466,100	Russia Russia	[46]
43	Verizon	Telecommunications	$130,863	Increase $15,528	144,500	United States United States	[47]
44	Bank of China	Financials	$127,714	Increase $27,255	310,119	China China	[48]
45	Allianz	Financials	$126,779	Increase $8,806	142,460	Germany Germany	[49]
46	AXA	Financials	$125,578	Increase $2,525	104,065	France France	[50]
47	Kroger	Retail	$121,162	Increase $3,110	453,000	United States United States	[51]
48	General Electric	Conglomerate	$120,268	Decrease -$22,355	283,000	United States United States	[52]
49	Fannie Mae	Financials	$120,101	Increase $15,959	7,400	United States United States	[53]
50	Lukoil	Oil and gas	$119,145	Increase $9,863	102,500	Russia Russia	[54]
